<?php
/**
 * Created by PhpStorm.
 * User: toshiba
 * Date: 3/21/2018
 * Time: 5:59 PM
 */


include_once "vendor/autoload.php";
use Pondit\Library\Publisher;
use Pondit\Library\Author;
use Pondit\Library\Book;

$book=new Book();
echo "Book Name:";
echo  $book->bookName;
echo "<br>";
echo "Author Name:";
$author=new Author();
echo $author->authorName;
echo "<br>";
$publisher=new Publisher();
echo "Publisher Name:";
echo $publisher->publisherName;

